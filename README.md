# Magento2 Notes

1. [ENV](#env)
2. [Change URL](#url)
3. [Register link  Remove](#link_remove)
4. [Admin pannel](#admin)
5. [Add plugin](#plugin)
6. [Email](#email)
7. [Banner Slider](#slider)
8. [Mangento CLI](#cli)
9. [Troubleshooting](#troubleshooting)
10. [Custom CSS](#css)



## 1. env <a name="env"></a>
```
    app/etc/env.php
```

## 2. Change URL by cli <a name="url"></a>

```
    bin/magento setup:store-config:set --base-url=http://1.1.1.1
```


## Change URL by Mysql
```
select * from core_config_data where path like '%base%url%';

update core_config_data set value = 'http://domainname/' where path = 'web/unsecure/base_url';
update core_config_data set value = 'http://domainname/' where path = 'web/secure/base_url';

```

## 3. Register link  Remove from Frontend <a name="link_remove"></a>
```
    app/design/frontend/Magento/luma/Magento_Theme/layout/

    ```` 
        <referenceBlock name="register-link" remove="true" />           <!--for Create Account Link-->
        <referenceBlock name="authorization-link-login" remove="true" />
    ````
```
## Register link Remove from Backend

``` 
app/code/Magento/Customer/Model/Registration.php

public function isAllowed()
    {
        return false;      // Register လုပ်တာကို ပိတ်နိုင်တယ် 
    }
```


## 4. Change Admin pannel Logo and icon <a name="admin"></a>

```
1. lib/web/images/magento-logo.svg
2. setup/pub/images/magento-logo.svg
3. pub/static/adminhtml/Magento/backend/en_US/images/magento-logo.svg

4. app/design/adminhtml/Magento/backend/web/images/magento-icon.svg
5. pub/static/adminhtml/Magento/backend/en_US/images/magento-icon.svg


```

## Copyrigt 

```

app/code/Magento/Backend/view/adminhtml/templates/page/copyright.phtml 


<?= $block->escapeHtml(__('Copyright &copy; %1 ABC MIB Group. All rights reserved.', date('Y'))) ?>

```

## 5. Add plugin  <a name="plugin"></a>


```
copy plugin magento/app/code/

php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
chown -R www-data:www-data ./

composer require/remove mageplaza/module-smtp

# Delete Order Plugin
composer require mageplaza/module-delete-orders
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```
### 6. Email  <a name="email"></a>

```
General > Store Email Addresses > Sender name & Sender Email
Store> Config > Enable SMTP
Protocol > SSL
Auth > LOGIN

Store> Config> Customers > Customer Config >Require Emails Confirmation > Yes

```

## 7. Banner Slider <a name="slider"></a>

```
copy plugin magento/app/code/

php bin/magento module:enable WeltPixel_Backend --clear-static-content
php bin/magento module:enable WeltPixel_OwlCarouselSlider --clear-static-content

php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
chown -R www-data:www-data ./

```
## 8. Mangento CLI <a name="cli"></a>

### Clear cache

```
    php bin/magento cache:clean && php bin/magento cache:flush
```

### Enable Developer Mode
```	
php bin/magento deploy:mode:set developer
```

## 9. Troubleshooting  <a name="troubleshooting"></a> 

### Customer is not displaying
```
php -dmemory_limit=512M bin/magento indexer:reindex customer_grid
```

### CatalogSearch

```
 php -dmemory_limit=512M bin/magento indexer:reindex catalogsearch_fulltext
```

### Other index
```
php -dmemory_limit=512M bin/magento indexer:reindex catalog_product_attribute

php -dmemory_limit=512M bin/magento indexer:reindex cataloginventory_stock
```
## 10. Custom CSS  <a name="css"></a> 

```
/pub/static/adminhtml/Magento/backend/en_US/css/styles.css
```